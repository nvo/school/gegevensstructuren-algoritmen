#ifndef SUDOKU_H
#define SUDOKU_H

#include <fstream>
#include <iostream>

class Sudoku
{
public:
    Sudoku(const std::string filename);
    bool isSolved() const;
    bool solve(int = 0, int = -1);
    friend std::ostream &operator<<(std::ostream &out, const Sudoku &s);

    /**
     * Get the content of a cell.
     *
     * @param r The cell's row.
     * @param c The cell's column.
     *
     * @return The cell's content.
     */
    int get_cell(int r, int c) const;

private:
    int grid[9][9];
    bool valid;

    /**
     * Checks whether a given value in a given cell is valid.
     *
     * @param x The value to check.
     * @param row The row of the cell to insert the value into.
     * @param col The column of the cell to insert the value into.
     *
     * @return Whether the value is valid in the given cell.
     */
    bool isValidCell(int x, int row, int col) const;

    /**
     * Checks whether the sudoku in its entirety is valid.
     *
     * @return Whether the sudoku is valid.
     */
    bool isValid() const;
};

#endif