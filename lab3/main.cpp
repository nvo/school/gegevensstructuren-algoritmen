#include <iostream>
#include <cassert>
#include <string>
#include "Sudoku.h"

// uncomment next line to enable debugging output
//#define ENABLE_DEBUG

void test_isSolved_succeeds()
{
    Sudoku solved("lab3/1_solved.txt");
    assert(solved.isSolved());
}

void test_isSolved_fails()
{
    Sudoku notSolved("lab3/1_almost_solved.txt");
    assert(!notSolved.isSolved());
}

void test_isSolved()
{
    test_isSolved_succeeds();
    test_isSolved_fails();
}

/**
 * ALGORITHM
 * The implemented algorithm makes use of recursive backtracking to solve the given sudoku.
 * First the algorithm searches for an empty cell, represented by a 0.
 *
 * Once an empty cell is found it can be filled with a number.
 * Only valid numbers are inserted, this means numbers not present in the corresponding row, column, and subgrid.
 *
 * Once the number is inserted, the function is run recursively, with a new starting position to search for the next empty cell.
 * If there are no valid numbers for the selected cell, that means that this branch of the decision tree is wrong.
 * When backtracking out of a wrong branch, the selected cell is reset to an empty cell.
 *
 * This is run until the sudoku is solved. Once a solution is found, the algorithm stops.
 *
 *
 * ARCHITECTURE
 * main.cpp
 * This file contains the main method, which runs some tests regarding the isSolved() method from the Sudoku class, and solves the sudokus provided in the sudokus folder.
 * To test whether the sudokus are solved properly, the upper-left three-digit numbers for each solved sudoku are summed, this should equal to 24702.
 * Solving the sudoku is done by calling the solve() method on a Sudoku object.
 *
 *
 * Sudoku.h
 * Contains the declaration of the Sudoku class and its properties and methods.
 *
 *
 * Sudoku.cpp
 * Contains the implementation of the Sudoku class' methods.
 *
 * The public solve() method is used to solve the Sudoku object.
 * The method takes 2 optional parameters: prevRow (default = 0) and preCol (default = -1).
 * These parameters are used for an optimization to limit the amount cells needed to be checked before finding an empty cell.
 * The solve() method is called within the main method, the solve() method itself calls the private isValidCell() method and the public isSolved() method.
 *
 * The public isSolved() method is used to check whether the Sudoku object is solved.
 * The isSolved() method is called within the tests in main.cpp and within the solve() method.
 *
 * The private isValidCell() method is used to check whether a certain value would be valid on a certain cell.
 * The isValidCell() method is called within the solve() method, so only valid numbers are tried.
 */
int main()
{
    // run tests
    test_isSolved();

    int sum = 0;
    const int expected_sum = 24702;
    // run main program
    for (int i = 1; i <= 50; i++)
    {
        Sudoku s("lab3/sudokus/" + std::to_string(i) + ".txt");

#ifdef ENABLE_DEBUG
        std::cout << "SUDOKU #" << i << std::endl;
        std::cout << "BEFORE:" << std::endl;
        std::cout << s << std::endl;
#endif

        // solve
        assert(s.solve());
        sum += s.get_cell(0, 0) * 100 + s.get_cell(0, 1) * 10 + s.get_cell(0, 2);

#ifdef ENABLE_DEBUG
        std::cout
            << "AFTER:" << std::endl;
        std::cout << s << std::endl
                  << std::endl;
#endif
    }

    std::cout << "Sum: " << sum << std::endl
              << std::endl;
    assert(sum == expected_sum);

    // Diagonal sudoku
    Sudoku diagonal("lab3/diagonal.txt");
    diagonal.solve();

#ifdef ENABLE_DEBUG
    std::cout << "DIAGONAL: " << std::endl;
    std::cout << diagonal << std::endl
              << std::endl;
#endif

    // Empty sudoku
    Sudoku zeros("lab3/zeros.txt");
    zeros.solve();

#ifdef ENABLE_DEBUG
    std::cout << "ZEROS: " << std::endl;
    std::cout << zeros << std::endl
              << std::endl;
#endif

    // Invalid sudoku
    Sudoku invalid("lab3/invalid.txt");
    invalid.solve();

#ifdef ENABLE_DEBUG
    std::cout << "INVALID: " << std::endl;
    std::cout << invalid << std::endl
              << std::endl;
#endif
}
