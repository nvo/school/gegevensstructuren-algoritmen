#include "Sudoku.h"

std::ostream &operator<<(std::ostream &out, const Sudoku &s)
{
  for (int i = 0; i < 9; i++)
  {
    for (int j = 0; j < 9; j++)
    {
      out << s.grid[i][j] << " ";

      if ((j + 1) % 3 == 0 && j < 8)
        out << "| ";
    }
    out << std::endl;
    if ((i + 1) % 3 == 0 && i < 8)
    {
      out << "------+-------+------" << std::endl;
    }
  }
  return out;
}

Sudoku::Sudoku(const std::string filename)
{
  std::ifstream infile(filename);
  if (infile)
  {
    for (int i = 0; i < 9; i++)
    {
      for (int j = 0; j < 9; j++)
      {
        infile >> grid[i][j];
      }
    }

    valid = isValid();
  }
}

bool Sudoku::isSolved() const
{
  // the sudoku can't be solved if it is invalid
  if (!valid)
    return false;

  const unsigned short solved = (1 << 9) - 1;
  short row;
  short col;
  short grd;
  int x;

  for (int i = 0; i < 9; i++)
  {
    row = 0;
    col = 0;
    grd = 0;

    for (int j = 0; j < 9; j++)
    {
      // check rows
      x = grid[i][j];
      if (x < 1)
        return false;
      row ^= 1 << (x - 1);

      // check columns
      x = grid[j][i];
      if (x < 1)
        return false;
      col ^= 1 << (x - 1);

      // check subgrids
      x = grid[3 * (i / 3) + (j / 3)][3 * (i % 3) + (j % 3)];
      if (x < 1)
        return false;
      grd ^= 1 << (x - 1);
    }

    if (row != solved || col != solved || grd != solved)
      return false;
  }

  return true;
}

bool Sudoku::solve(int prevRow, int prevCol)
{
  // don't do anything if the sudoku is invalid
  if (!valid)
    return false;

  // check whether the sudoku is solved already
  if (isSolved())
    return true;

  int r = prevRow, c = prevCol;
  // get first empty cell
  while (
      // increment c until it's >= 9
      // when c >= 9: set c back to zero and increment r until it's >= 9
      (++c < 9 || (c = 0) || ++r < 9) &&

      // check if the cell contains a 0
      grid[r][c] > 0)
    ;

  for (int x = 1; x <= 9; x++)
  {
    // if x in the cell is invalid, just skip it
    if (!isValidCell(x, r, c))
      continue;

    // set the cell to x
    grid[r][c] = x;

    // recursively solve the sudoku
    if (solve(r, c))
      return true;
  }

  // reset the cell to 0
  grid[r][c] = 0;
  return false;
}

bool Sudoku::isValidCell(int value, int row, int col) const
{
  // start values for the subgrid
  int grd_r_start = 3 * (row / 3);
  int grd_c_start = 3 * (col / 3);

  for (int i = 0; i < 9; i++)
  {
    int grd_r = grd_r_start + (i / 3);
    int grd_c = grd_c_start + (i % 3);
    if (
        // check the entire row
        (i != col && grid[row][i] == value) ||

        // check the entire column
        (i != row && grid[i][col] == value) ||

        // check the entire subgrid
        (grd_r != row && grd_c != col && grid[grd_r][grd_c] == value))
    {
      // when the same value is found in either the row, column, or subgrid
      // => invalid
      return false;
    }
  }
  // if nothing is found
  // => valid
  return true;
}

bool Sudoku::isValid() const
{
  for (int r = 0; r < 9; r++)
    for (int c = 0; c < 9; c++)
      if (grid[r][c] > 0 && !isValidCell(grid[r][c], r, c))
        return false;
  return true;
}

int Sudoku::get_cell(int r, int c) const
{
  return grid[r][c];
}
