#ifndef SHELLSORT_H
#define SHELLSORT_H

#include "sorteermethode.h"
#include <iostream>

/** \class Shellsort
*/
template <class T>
class ShellSort : public Sorteermethode<T>
{
public:
    ShellSort();

    void operator()(vector<T> &v) const;
};

template <class T>
ShellSort<T>::ShellSort()
{
}

template <class T>
void ShellSort<T>::operator()(vector<T> &v) const
{
    int k = v.size() / 2;

    while (k >= 1)
    {
        for (int i = k; i < v.size(); ++i)
        {
            T tmp = std::move(v[i]);
            int j = i - k;

            while (j >= 0 && tmp < v[j])
            {
                v[j + k] = std::move(v[j]);
                j -= k;
            }
        }

        k /= 2;
    }
}

#endif
