#ifndef INSERTIONSORT_H
#define INSERTIONSORT_H

#include "sorteermethode.h"

/** \class InsertionSort
*/
template <class T>
class InsertionSort : public Sorteermethode<T>
{
public:
    InsertionSort();

    void operator()(vector<T> &v) const;
};

template <class T>
InsertionSort<T>::InsertionSort()
{
}

template <class T>
void InsertionSort<T>::operator()(vector<T> &v) const
{
    for (int i = 1; i < v.size(); ++i)
    {
        T tmp = std::move(v[i]);
        int j = i - 1;

        while (j >= 0 && tmp < v[j])
        {
            v[j + 1] = std::move(v[j]);
            --j;
        }
        v[j + 1] = std::move(tmp);
    }
}

#endif
