#ifndef MERGESORT_H
#define MERGESORT_H

#include "sorteermethode.h"

/** \class MergeSort
*/
template <class T>
class MergeSort : public Sorteermethode<T>
{
public:
    MergeSort();

    void operator()(vector<T> &v) const;

private:
    void mergesort(vector<T> &v, int l, int r, vector<T> &tmp) const;
    void merge(vector<T> &v, int l, int m, int r, vector<T> &tmp) const;
};

template <class T>
MergeSort<T>::MergeSort()
{
}

template <class T>
void MergeSort<T>::operator()(vector<T> &v) const
{
    vector<T> tmp(v.size());
    this->mergesort(v, 0, v.size(), tmp);
}

template <class T>
void MergeSort<T>::mergesort(vector<T> &v, int l, int r, vector<T> &tmp) const
{
    if (l < r - 1)
    {
        int m = l + (r - l) / 2;
        this->mergesort(v, l, m, tmp);
        this->mergesort(v, m, r, tmp);
        this->merge(v, l, m, r, tmp);
    }
}

template <class T>
void MergeSort<T>::merge(vector<T> &v, int l, int m, int r, vector<T> &tmp) const
{
    // move half of v to tmp
    for (int i = l; i < m; ++i)
    {
        tmp[i] = std::move(v[i]);
    }

    int a = m;
    int b = l;
    int index = l;

    while (a < r && b < m)
    {
        v[index++] = v[a] < tmp[b] ? std::move(v[a++]) : std::move(tmp[b++]);
    }

    while (r - a > 0)
    {
        v[index++] = std::move(v[a++]);
    }

    while (m - b > 0)
    {
        v[index++] = std::move(tmp[b++]);
    }
}

#endif
