#ifndef STLSORT_H
#define STLSORT_H

#include "sorteermethode.h"
#include <algorithm>

/** \class STLSort
 * Sorts a vector using the STL sort function
*/
template <class T>
class STLSort : public Sorteermethode<T>
{
public:
    STLSort();

    void operator()(vector<T> &v) const;
};

template <class T>
STLSort<T>::STLSort()
{
}

template <class T>
void STLSort<T>::operator()(vector<T> &v) const
{
    std::sort(v.begin(), v.end());
}

#endif
