#ifndef SUDOKU_H
#define SUDOKU_H

#include <fstream>
#include <iostream>

class Sudoku{
public:
    Sudoku(const std::string filename);
    bool isSolved();
    bool solve();
    friend std::ostream& operator<< (std::ostream &out, const Sudoku &s);
private:
    int grid[9][9];
};

std::ostream& operator<< (std::ostream &out, const Sudoku &s){
    for (int i = 0; i < 9; i++){
        for (int j = 0; j < 9; j++){
            out << s.grid[i][j] << " " ;

            if ((j+1) % 3 == 0 && j < 8)
                out << "| ";
        }
        out << std::endl;
        if ((i+1) % 3 == 0 && i < 8){
            out << "------+-------+------" << std::endl;
        }
    }
    return out;
}

Sudoku::Sudoku(const std::string filename){
    std::ifstream infile(filename);
    if (infile){
        for (int i = 0; i < 9; i++){
            for (int j = 0; j < 9; j++){
                infile >> grid[i][j];
            }
        }
    }
}

bool Sudoku::isSolved(){
    return true;
}


bool Sudoku::solve(){
    return true;
}

#endif